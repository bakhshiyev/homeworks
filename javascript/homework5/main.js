// Markup for tabs is in the folder tabs. It is necessary to display a specific text for the each tab by clicking on it.
// The rest of the text must be hidden. In the comments you can see which text should be displayed for which tab.
// Markup can be changed, you can add some classes, id, attributes and tags.

function showContent(e, name) {
    let tabs = document.body.querySelector('.tabs').children;
    let contents = document.body.querySelector('.tabs-content').children;

    for (let i = 0; i < contents.length; i++) {
        contents[i].style.display = "none";
    }

    for (let i = 0; i < tabs.length; i++) {
        tabs[i].className = tabs[i].className.replace(" active", "");
    }

    document.getElementById(name).style.display = "block";
    e.currentTarget.className += " active";
    console.log(e.currentTarget);
}

function addContent(){
    let tab=document.body.querySelector('#tab').value;
    let content=document.body.querySelector('#content').value;
    
    let tabs = document.body.querySelector('.tabs');
    let contents = document.body.querySelector('.tabs-content');

    let listElement=document.createElement("LI");
    listElement.textContent=tab;
    listElement.classList.add('tabs-title');
    tabs.append(listElement);
}



// It is necessary to provide the possibility to change the text on the tabs, and also to add and delete the tabs.
// It is necessary that the function written in the javascript does not stop working because of these changes.


