//Use the modal browser window to read user credentials: name and age.
let name=prompt("Include your name!");
let age=+prompt("Include your age!");

//If the age is less than 18 years - show the message: You are not allowed to visit this website.
if(age<18){
    alert("You are not allowed to visit this website");
}

//If the age of 18 to 22 years (inclusive) - show the window with the following message: Are you sure you want to continue? and buttons Ok, Cancel.
//If the user has pressed Ok, display a message on the screen: Welcome,  + username. If the user has clicked Cancel, display a message on the screen: You are not allowed to visit this website.
else if(age>=18 && age<=22){
    if(confirm("Are you sure you want to continue"))
        alert(`Welcome, ${name}`);
    else
        alert("You are not allowed to visit this website");
}

//If users age is over 22 years old, display the following message on the screen: Welcome,  + username.
else if(age>22){
    alert(`Welcome, ${name}`);
}
//Be sure to use the ES6 syntax (ES2015) when creating variables.


