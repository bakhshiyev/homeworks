// Implement the function to filter the array by the specified data type.

/*
Create a function filterBy(), which will accept 2 arguments.
The first argument is an array that will contain any data, the second argument is a data type.
*/

let arr = ['hello', 'world', 23, '23', null];
let specifiedData = prompt("Include desired data");

function filterBy(arr, dataType) {

    /*
    The function must return a new array that will contain all the data that has been passed to the argument,
    except the data with the type that has been passed by the second argument.
     */
    let newArr = [];
    let k = 0;

    for (let i = 0; i < arr.length; i++) {
        if (typeof (arr[i]) != dataType) {
            newArr[k]=arr[i];
            k++;
        }
    }
    console.log(newArr);
}

filterBy(arr,specifiedData);


/*
That means, if you put the ['hello', 'world', 23, '23', null] array
into the first argument and put 'string' into the second argument,
the function will return the array [23, null].
 */