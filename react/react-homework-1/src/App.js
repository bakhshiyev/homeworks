import React from 'react';
import './style.sass';
import cross from './cross.svg'

class Button extends React.Component {
  render() {
    return (
      <button onClick={this.props.onClick} style={{ backgroundColor: this.props.backgroundColor }}>{this.props.text}</button>
    );
  }
}

class Modal extends React.Component {
  render() {
    // if(this.props.status==true)
    return (
      <div className="modal-container">
        <div className="header">
          <h5>{this.props.header}</h5>
          {this.props.closeButton && <img src={cross} alt="close image" />}
        </div>
        <div className="content">
          <p>{this.props.text}</p>
          <div className="buttons">
            <button>{this.props.actions[0]}</button>
            <button>{this.props.actions[1]}</button>
          </div>
        </div>
      </div>
    );
  }
}

const handleClick = () => {
  console.log("hello");
  document.body.style.backgroundColor = "rgba(0,0,0,0.85)";
}

function App() {
  return (
    <div>
      <div className="container">
        <Button
          status={false}
          backgroundColor="#116979"
          text="open first modal"
          onClick={handleClick} />
        <Button
          status={true}
          backgroundColor="#18b0b0"
          text="open second modal"
          onClick={handleClick} />
      </div>
      <Modal
        status={false}
        header="Do you want to delete this file?"
        closeButton={true}
        text="Once you delete this file, it won’t be possible to undo this action. Are you sure you want to delete it?"
        actions={['OK', 'Cancel']} />
    </div>
  );
}

export default App;
